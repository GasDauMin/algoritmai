﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmai
{
    class Fio
    {
        protected FileStream fsWork; //File stream for work with arrays
        protected FileStream fsResult; //File stream for results

        protected BinaryReader binaryReader;
        protected BinaryWriter binaryWriter;

        protected StreamWriter swFile; //Result output stream to file
        protected StreamWriter swConsole; //Result output stream to console

        protected bool stf; //Need stream to file?
        protected bool stc; //Need stream to console?
        protected bool nozero; //Need print undefined elements?

        protected string tbp, tap; //TextBeforePrint,TextAfterPrint

        protected int curentAddress; // Dynamic cursor
        protected bool optimized; // Optimize list for smaller amount of iterations

        //Debugg mode
        protected Int64 iterations;
        protected bool countIterations;

        public Fio(String inputFile)
        {
            string uniqueInputFile = string.Format(@inputFile+"_{0}.arr", Guid.NewGuid());
            this.fsWork = new FileStream(uniqueInputFile, FileMode.Create, FileAccess.ReadWrite);
            this.binaryWriter = new BinaryWriter(this.fsWork);
            this.binaryReader = new BinaryReader(this.fsWork);
            this.swConsole = new StreamWriter(Console.OpenStandardOutput());
            this.swConsole.AutoFlush = true;
            //Set default print parameters
            this.stc = true; this.stf = false; this.nozero = true;
            this.tbp = ""; this.tap = "";
            this.curentAddress = 0;
            //Debugg mode
            this.iterations = 0;
            this.countIterations = false;
        }

        public Fio(String inputFile, String outputFile)
        {
            string uniqueInputFile = string.Format(@inputFile + "_{0}.arr", Guid.NewGuid());
            this.fsWork = new FileStream(uniqueInputFile, FileMode.Create, FileAccess.ReadWrite);
            this.fsResult = new FileStream(outputFile, FileMode.Create, FileAccess.Write);
            this.binaryWriter = new BinaryWriter(this.fsWork);
            this.binaryReader = new BinaryReader(this.fsWork);
            this.swFile = new StreamWriter(fsResult);
            this.swFile.AutoFlush = true;
            this.swConsole = new StreamWriter(Console.OpenStandardOutput());
            this.swConsole.AutoFlush = true;
            //Set default print parameters
            this.stc = true; this.stf = true; this.nozero = true;
            this.tbp = ""; this.tap = "";
            this.curentAddress = 0;
            //Debugg mode
            this.iterations = 0;
            this.countIterations = false;
        }

        public Fio(String inputFile, FileStream outputFile, StreamWriter outputStream)
        {
            string uniqueInputFile = string.Format(@inputFile + "_{0}.arr", Guid.NewGuid());
            this.fsWork = new FileStream(uniqueInputFile, FileMode.Create, FileAccess.ReadWrite);
            this.fsResult = outputFile;
            this.binaryWriter = new BinaryWriter(this.fsWork);
            this.binaryReader = new BinaryReader(this.fsWork);
            this.swFile = outputStream;
            this.swFile.AutoFlush = true;
            this.swConsole = new StreamWriter(Console.OpenStandardOutput());
            this.swConsole.AutoFlush = true;
            //Set default print parameters
            this.stc = true; this.stf = true; this.nozero = true;
            this.tbp = ""; this.tap = "";
            this.curentAddress = 0;
            //Debugg mode
            this.iterations = 0;
            this.countIterations = false;
        }

        // Work with files
        //###################################################################################################
        public void cleanWorkFile()
        {
            this.fsWork.SetLength(0);
            this.fsWork.Flush();
        }

        public void cleanResultFile()
        {
            this.fsResult.SetLength(0);
            this.fsResult.Flush();
        }

        // Results
        public void smartWriteLine(string text, bool condition = true)
        {
            if (condition)
            {
                if (this.stf)
                {
                    Console.SetOut(this.swFile);
                    Console.Out.WriteLine(text);
                }
                if (this.stc)
                {
                    Console.SetOut(this.swConsole);
                    Console.Out.WriteLine(text);
                }
            }
            Console.SetOut(this.swConsole);
        }

        public void smartWrite(string text, bool condition = true)
        {
            if (condition)
            {
                if (this.stf)
                {
                    Console.SetOut(this.swFile);
                    Console.Out.Write(text);
                }
                if (this.stc)
                {
                    Console.SetOut(this.swConsole);
                    Console.Out.Write(text);
                }
            }
            Console.SetOut(this.swConsole);
        }

        // Misc options
        //###################################################################################################
        public void setTextBeforePrint(string text)
        { // Set tbp value
            this.tbp = text;
        }

        public void setTextAfterPrint(string text)
        { // Set tap value
            this.tap = text;
        }

        public void setDefaultTextParams()
        { // Set defaults (TextBeforePrint,TextAfterPrint)
            this.tbp = ""; this.tap = "";
        }

        public void enableZeros()
        { // Print undefined values as 0 until last element
            this.nozero = false;
        }

        public void disableZeros()
        { // Do not print undefined values
            this.nozero = true;
        }

        public void enableConsolePrint()
        { // Print to console
            this.stc = true;
        }

        public void disableConsolePrint()
        { // Do not print to console
            this.stc = false;
        }

        public void enableFilePrint()
        { // Print to file
            this.stf = true;
        }

        public void disableFilePrint()
        { // Do not print to file
            this.stf = false;
        }

        public void enableOptimization()
        { // List optimization enable
            this.optimized = true;
        }

        public void disableOptimization()
        { // List optimization disable
            this.optimized = true;
        }

        public void enableIterations()
        { // Enable counting iterations
            this.countIterations = true;
        }

        public void disableIterations()
        { // Disable counting iterations
            this.countIterations = false;
        }

        public Int64 getIterations(int except=0)
        {
            return iterations - except;
        }
    }
}
