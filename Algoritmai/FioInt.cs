﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmai
{
    class FioInt : Fio
    {
        public FioInt(String inputFile)
            : base(inputFile)
        {
        }

        public FioInt(String inputFile, String outputFile)
            : base(inputFile, outputFile)
        {
        }

        public FioInt(String inputFile, FileStream outputFile, StreamWriter outputStream)
            : base(inputFile, outputFile, outputStream)
        {
        }

        // Main work with array functions
        //###################################################################################################
        public int get(int position)
        {
            try
            {
                this.binaryReader.BaseStream.Seek(position * sizeof(int), System.IO.SeekOrigin.Begin);
                //Counting iterations
                if (this.countIterations) this.iterations += 1;
                return this.binaryReader.ReadInt32();
            }
            catch
            {
                return -1;
            }
        }

        public void set(int position, int number)
        {
            this.binaryWriter.Seek(position * sizeof(int), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void swap(int positionA, int positionB)
        {
            //Read elements
            this.binaryReader.BaseStream.Seek(positionA * sizeof(int), System.IO.SeekOrigin.Begin);
            int a = this.binaryReader.ReadInt32();
            this.binaryReader.BaseStream.Seek(positionB * sizeof(int), System.IO.SeekOrigin.Begin);
            int b = this.binaryReader.ReadInt32();
            //Write swaped elements
            this.binaryWriter.Seek(positionA * sizeof(int), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(b);
            this.binaryWriter.Seek(positionB * sizeof(int), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(a);
            //Counting iterations
            if (this.countIterations) this.iterations += 8;
        }

        public void remove(int position)
        {
            this.binaryWriter.Seek(position * sizeof(int), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        // Dynamic use
        public void declare(int size)
        {
            this.binaryWriter.Seek(size * sizeof(int), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goFirst()
        {
            this.curentAddress = 0; // Put cursor to first position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goNext()
        {
            this.curentAddress += sizeof(int); // Put cursor to next position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goPrev()
        {
            this.curentAddress -= sizeof(int); // Put cursor to previous position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goPosition(int position)
        {
            if (this.optimized)
            {
                int currentPosition = this.curentAddress / sizeof(int);
                if (currentPosition < position)
                    for (int i = currentPosition; i != position; i++)
                        goNext();
                else if (currentPosition > position)
                    for (int i = currentPosition; i != position; i--)
                        goPrev();
            }
            else
            {
                goFirst();
                for (int i = 0; i < position; i++)
                    goNext();
            }
        }

        public int goGet()
        {
            try
            {
                this.binaryReader.BaseStream.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
                //Counting iterations
                if (this.countIterations) this.iterations += 2;
                return this.binaryReader.ReadInt32();
            }
            catch
            {
                return -1;
            }
        }

        public int goGet(int position)
        {
            goPosition(position);
            return goGet();
        }

        public void goRemove()
        {
            this.binaryWriter.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goRemove(int position)
        {
            goPosition(position);
            goRemove();
        }

        public void goSet(int number)
        {
            this.binaryWriter.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goSet(int position, int number)
        {
            goPosition(position);
            goSet(number);
        }

        public void goPush(int number)
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            this.binaryWriter.Seek(length, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 3;
        }

        public bool goIsNextLast()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            int nextPosition = this.curentAddress + sizeof(int);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return (length < nextPosition);
        }

        public bool goIsInPosition(int position)
        {
            int currentPosition = this.curentAddress / sizeof(int);
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
            return (currentPosition == position);
        }

        // Misc functions
        public int countNonZeros()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            int element;
            int cursor = 0;
            int count = 0;
            while (length != 0)
            {
                this.binaryReader.BaseStream.Seek(cursor * sizeof(int), System.IO.SeekOrigin.Begin);
                element = this.binaryReader.ReadInt32();
                if (element != 0) { count++; }
                length -= sizeof(int);
                cursor++;
            }
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return count;
        }

        public int count()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return length / sizeof(int);
        }

        public FioInt join(FioInt fioInt)
        {
            for (fioInt.goFirst(); !fioInt.goIsNextLast(); fioInt.goNext())
            {
                goPush(fioInt.goGet());
            }
            return this;
        }

        public FioInt rewrite(FioInt fioInt)
        {
            cleanWorkFile();
            for (fioInt.goFirst(); !fioInt.goIsNextLast(); fioInt.goNext())
            {
                goPush(fioInt.goGet());
            }
            return this;
        }

        // Generators
        public void generateArray(int min, int max, int count = 100)
        {
            cleanWorkFile();
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                set(i, random.Next(min, max));
            }
        }

        // User data
        //###################################################################################################
        public void readUserFile(String file, char delimiter = '\n')
        {
            cleanWorkFile();
            using (TextReader reader = File.OpenText(file))
            {
                string text;
                string[] bits;
                while ((text = reader.ReadLine()) != null)
                {
                    bits = text.Split(delimiter);
                    foreach (string bit in bits)
                    {
                        goPush(int.Parse(bit));
                    }
                }
            }
        }

        public void readUserData()
        {
            cleanWorkFile();
            Console.Write("Enter count of numbers: ");
            int count = int.Parse(Console.ReadLine());

            for (int i = 0; i < count; i++)
            {
                Console.Write((i+1) + "]");
                goPush(int.Parse(Console.ReadLine()));
            }
        }

        // Results
        //###################################################################################################
        public void print(bool withIndex = false)
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            int element;
            int cursor = 0;

            smartWrite(this.tbp);
            while (length != 0)
            {
                this.binaryReader.BaseStream.Seek(cursor * sizeof(int), System.IO.SeekOrigin.Begin);
                element = this.binaryReader.ReadInt32();

                smartWrite(cursor.ToString() + ". ", withIndex && (((this.nozero) && (element != 0)) || (!this.nozero)));
                smartWrite(element.ToString() + "\r\n", (((this.nozero) && (element != 0)) || (!this.nozero)));

                length -= sizeof(int);
                cursor++;
            }
            smartWrite(this.tap);
        }

        public void printInRange(int begin, int end, bool withIndex = false)
        {
            smartWrite(this.tbp);
            for (int i = begin; i <= end; i++)
            {
                smartWrite(i.ToString() + ". ", withIndex && (((this.nozero) && (get(i) != 0)) || (!this.nozero)));
                smartWrite(get(i).ToString() + "\r\n", (((this.nozero) && (get(i) != 0)) || (!this.nozero)));
            }
            smartWrite(this.tap);
        }

    }
}
