﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;

namespace Algoritmai
{
    class Program
    {
        static void Main(string[] args)
        {
            Sort sort = new Sort();
        // ###########################################################################################################
        start: //Set result file
            Console.Clear();
            Console.Write(">>> Enter result file name: ");
            String resultFile = Console.ReadLine();
            FileStream resultStream = new FileStream(resultFile + ".rez", FileMode.Create, FileAccess.ReadWrite);
            StreamWriter result = new StreamWriter(resultStream);

            // ###########################################################################################################
            bool list;
        algorithm: //Select sorting algorithm
            Console.Clear();
            Console.WriteLine(">>> Select sorting algorithm:");
            Console.WriteLine("  B] BubbleSort");
            Console.WriteLine("  Q] QuickSort");
            Console.WriteLine("  R] RadixSort");
            Console.WriteLine("  Bl] BubbleSort - list");
            Console.WriteLine("  Ql] QuickSort - list");
            Console.WriteLine("  Rl] RadixSort - list");
            Console.Write("Enter algorithms letters: ");
            String algorithm = Console.ReadLine();
            switch (algorithm.ToUpper())
            {
                case "B": algorithm = "BubbleSort"; list = false; break;
                case "Q": algorithm = "QuickSort"; list = false; break;
                case "R": algorithm = "RadixSort"; list = false; break;
                case "BL": algorithm = "BubbleSortList"; list = true; break;
                case "QL": algorithm = "QuickSortList"; list = true; break;
                case "RL": algorithm = "RadixSortList"; list = true; break;
                default:
                    Console.WriteLine("ERROR wrong answer: " + algorithm);
                    Console.ReadKey();
                    goto algorithm;
            }

        // ###########################################################################################################
        input: //Select input type
            Console.Clear();
            Console.WriteLine(">>> Select input type:");
            Console.WriteLine("  G] Generate");
            Console.WriteLine("  F] File");
            Console.WriteLine("  C] Console");
            Console.Write("Enter input type letter: ");
            String input = Console.ReadLine();
            FioInt array = new FioInt("cache/global-" + algorithm, resultStream, result); // Declare new integers array
            switch (input.ToUpper())
            {
                case "G": //Generate
                    Console.Write("Enter count of numbers: ");
                    int inCnt = int.Parse(Console.ReadLine());
                    Console.Write("Enter minimal number: ");
                    int inMin = int.Parse(Console.ReadLine());
                    Console.Write("Enter maximal number: ");
                    int inMax = int.Parse(Console.ReadLine());
                    array.generateArray(inMin, inMax, inCnt);
                    break;
                case "F": //File
                    Console.Write("Data file name: ");
                    string inFn = Console.ReadLine();
                    array.readUserFile(inFn);
                    break;
                case "C": //Console
                    array.readUserData();
                    break;
                default:
                    Console.WriteLine("ERROR wrong answer: " + input);
                    Console.ReadKey();
                    goto input;
            }

            // ###########################################################################################################
            String optimization = "<disabled optimization>"; array.disableOptimization();
        option: //Option
            Console.Clear();
            if (list) // List type
            {
                Console.WriteLine(">>> Select option:");
                Console.WriteLine("  G] LetsGO " + optimization);
                Console.WriteLine("  E] Enable optimizations (only for lists)");
                Console.WriteLine("  D] Disable optimizations (only for lists)");
                Console.Write("Enter selected option letter: ");
                String option = Console.ReadLine();

                switch (option.ToUpper())
                {
                    case "G": //LetsGO!
                        goto letsgo;
                    case "E": //Enable optimizations (only for lists)
                        array.enableOptimization(); optimization = "<enabled optimization>"; goto option;
                    case "D": //Disable optimizations (only for lists)
                        array.disableOptimization(); optimization = "<disabled optimization>"; goto option;
                    default:
                        Console.WriteLine("ERROR wrong answer: " + option);
                        Console.ReadKey();
                        goto option;
                }
            }
            else // Array type
            {
                goto letsgo;
            }


            // ###########################################################################################################
        letsgo: //letsgo!
            Console.Clear();
            Console.WriteLine("<START>");

            array.disableConsolePrint();
            array.enableFilePrint();

            result.WriteLine("[" + algorithm + "]");
            if (list) result.WriteLine(optimization);

            array.setTextBeforePrint("\r\n\r\nUnsorted array: \r\n");
            array.print();

            // Experiments zone
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            array.enableIterations();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            switch (algorithm)
            {
                // BubbleSort sorting algorithm
                case "BubbleSort": sort.bubbleSort(array); break;
                case "BubbleSortList": sort.bubbleSortList(array); break;
                // QuickSort sorting algorithm
                case "QuickSort": array.rewrite(sort.quickSort(array)); break;
                case "QuickSortList": array.rewrite(sort.quickSort(array)); break;
                // RadixSort sorting algorithm
                case "RadixSort": sort.radixSort(array); break;
                case "RadixSortList": sort.radixSortList(array); break;
                default: break; //Wrong sorting algorihm
            }
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                ts.Hours, ts.Minutes, ts.Seconds, ts.Milliseconds / 10);
            array.disableIterations();
            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

            array.setTextBeforePrint("\r\r\n\nSorted array: \r\n");
            array.print();

            result.WriteLine("");
            result.WriteLine("Time: " + elapsedTime);
            result.WriteLine("Iterations: " + array.getIterations());
            result.WriteLine("Count: " + array.countNonZeros());

            array.enableConsolePrint();
            array.disableFilePrint();
            array.print();
            Console.WriteLine();
            if (list) Console.WriteLine("Algorithm: " + algorithm + " " + optimization);
            Console.WriteLine("Time: " + elapsedTime);
            Console.WriteLine("Iterations: " + array.getIterations());
            Console.WriteLine("Count: " + array.countNonZeros());
            Console.WriteLine("<END>");
            Console.ReadKey();
            goto start;
        }
    }
}
