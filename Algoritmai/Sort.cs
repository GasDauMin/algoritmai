﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace Algoritmai
{
    class Sort
    {
        // [RadixSort]
        //###################################################################################################
        public void radixSort(FioInt fio)
        {
            //Declare digits
            FioInt digit0 = new FioInt("data/rs-digit0");
            FioInt digit1 = new FioInt("data/rs-digit1");
            FioInt digit2 = new FioInt("data/rs-digit2");
            FioInt digit3 = new FioInt("data/rs-digit3");
            FioInt digit4 = new FioInt("data/rs-digit4");
            FioInt digit5 = new FioInt("data/rs-digit5");
            FioInt digit6 = new FioInt("data/rs-digit6");
            FioInt digit7 = new FioInt("data/rs-digit7");
            FioInt digit8 = new FioInt("data/rs-digit8");
            FioInt digit9 = new FioInt("data/rs-digit9");

            //Get maximal length of value
            int maxLength = 0;
            int count = fio.count();

            for (int i = 0; i < count; i++)
            {
                if (maxLength < fio.get(i).ToString().Length)
                {
                    maxLength = fio.get(i).ToString().Length;
                }
            }

            // RadixSort algorithm
            FioInt curDigit = new FioInt("data/rs-curdigit");
            for (int i = 0; i < maxLength; i++)
            {
                for (int j = 0; j < count; j++)
                {
                    int digit = (int)((fio.get(j) % Math.Pow(10, i + 1)) / Math.Pow(10, i));

                    switch (digit)
                    {
                        case 0: digit0.goPush(fio.get(j)); break;
                        case 1: digit1.goPush(fio.get(j)); break;
                        case 2: digit2.goPush(fio.get(j)); break;
                        case 3: digit3.goPush(fio.get(j)); break;
                        case 4: digit4.goPush(fio.get(j)); break;
                        case 5: digit5.goPush(fio.get(j)); break;
                        case 6: digit6.goPush(fio.get(j)); break;
                        case 7: digit7.goPush(fio.get(j)); break;
                        case 8: digit8.goPush(fio.get(j)); break;
                        case 9: digit9.goPush(fio.get(j)); break;
                        default: break;
                    }

                    int index = 0;
                    // Resorting by digits
                    curDigit.rewrite(digit0); // Sort by digit 0
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit1); // Sort by digit 1
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit2); // Sort by digit 2
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit3); // Sort by digit 3
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit4); // Sort by digit 4
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit5); // Sort by digit 5
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit6); // Sort by digit 6
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit7); // Sort by digit 7
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit8); // Sort by digit 8
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                    curDigit.rewrite(digit9); // Sort by digit 9
                    for (int l = 0; l < curDigit.count(); l++) fio.set(index++, curDigit.get(l));
                }

                //Clear digits
                digit0.cleanWorkFile();
                digit1.cleanWorkFile();
                digit2.cleanWorkFile();
                digit3.cleanWorkFile();
                digit4.cleanWorkFile();
                digit5.cleanWorkFile();
                digit6.cleanWorkFile();
                digit7.cleanWorkFile();
                digit8.cleanWorkFile();
                digit9.cleanWorkFile();
            }
        }

        public void radixSortList(FioInt fio)
        {
            //Declare digits
            FioInt digit0 = new FioInt("data/rs-digit0");
            FioInt digit1 = new FioInt("data/rs-digit1");
            FioInt digit2 = new FioInt("data/rs-digit2");
            FioInt digit3 = new FioInt("data/rs-digit3");
            FioInt digit4 = new FioInt("data/rs-digit4");
            FioInt digit5 = new FioInt("data/rs-digit5");
            FioInt digit6 = new FioInt("data/rs-digit6");
            FioInt digit7 = new FioInt("data/rs-digit7");
            FioInt digit8 = new FioInt("data/rs-digit8");
            FioInt digit9 = new FioInt("data/rs-digit9");

            //Get maximal length of value
            int maxLength = 0;
            int count = fio.count();

            for (fio.goFirst(); !fio.goIsNextLast(); fio.goNext())
            {
                if (maxLength < fio.goGet().ToString().Length)
                {
                    maxLength = fio.goGet().ToString().Length;
                }
            }

            // RadixSort algorithm
            FioInt curDigit = new FioInt("data/rs-curdigit");
            for (int i = 0; i < maxLength; i++)
            {
                for (fio.goFirst(); !fio.goIsNextLast(); fio.goNext())
                {
                    int digit = (int)((fio.goGet() % Math.Pow(10, i + 1)) / Math.Pow(10, i));

                    switch (digit)
                    {
                        case 0: digit0.goPush(fio.goGet()); break;
                        case 1: digit1.goPush(fio.goGet()); break;
                        case 2: digit2.goPush(fio.goGet()); break;
                        case 3: digit3.goPush(fio.goGet()); break;
                        case 4: digit4.goPush(fio.goGet()); break;
                        case 5: digit5.goPush(fio.goGet()); break;
                        case 6: digit6.goPush(fio.goGet()); break;
                        case 7: digit7.goPush(fio.goGet()); break;
                        case 8: digit8.goPush(fio.goGet()); break;
                        case 9: digit9.goPush(fio.goGet()); break;
                        default: break;
                    }

                    int index = 0;
                    // Resorting by digits
                    curDigit.rewrite(digit0); // Sort by digit 0
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit1); // Sort by digit 1
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit2); // Sort by digit 2
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit3); // Sort by digit 3
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit4); // Sort by digit 4
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit5); // Sort by digit 5
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit6); // Sort by digit 6
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit7); // Sort by digit 7
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit8); // Sort by digit 8
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                    curDigit.rewrite(digit9); // Sort by digit 9
                    for (curDigit.goFirst(); !curDigit.goIsNextLast(); curDigit.goNext()) fio.set(index++, curDigit.goGet());
                }

                //Clear digits
                digit0.cleanWorkFile();
                digit1.cleanWorkFile();
                digit2.cleanWorkFile();
                digit3.cleanWorkFile();
                digit4.cleanWorkFile();
                digit5.cleanWorkFile();
                digit6.cleanWorkFile();
                digit7.cleanWorkFile();
                digit8.cleanWorkFile();
                digit9.cleanWorkFile();
            }
        }

        // [QuickSort]
        //###################################################################################################
        public FioInt quickSort(FioInt fio)
        {
            FioInt lo = new FioInt("data/qs-lo");
            FioInt gt = new FioInt("data/qs-gt");
            FioInt results = new FioInt("data/qs-results");

            if (fio.countNonZeros() < 2)
            {
                return fio;
            }
            int pivot = 0;
            int pivot_value = fio.get(pivot);
            fio.remove(pivot);

            int count = fio.count();
            for (int i = 0; i < count; i++)
            {
                if (fio.get(i) <= pivot_value)
                    lo.goPush(fio.get(i));
                else
                    gt.goPush(fio.get(i));
            }

            lo = quickSort(lo);
            gt = quickSort(gt);

            results.join(lo);
            results.goPush(pivot_value);
            results.join(gt);
            return results;
        }

        public FioInt quickSortList(FioInt fio)
        {
            FioInt lo = new FioInt("data/qs-lo");
            FioInt gt = new FioInt("data/qs-gt");
            FioInt results = new FioInt("data/qs-results");

            if (fio.countNonZeros() < 2)
            {
                return fio;
            }

            fio.goFirst();
            int pivot_value = fio.goGet();
            fio.goRemove();

            for (fio.goFirst(); !fio.goIsNextLast(); fio.goNext())
            {
                if (fio.goGet() <= pivot_value)
                    lo.goPush(fio.goGet());
                else
                    gt.goPush(fio.goGet());
            }

            lo = quickSort(lo);
            gt = quickSort(gt);

            results.join(lo);
            results.goPush(pivot_value);
            results.join(gt);
            return results;
        }

        // [BubbleSort]
        //###################################################################################################
        public void bubbleSort(FioInt fio)
        {
            bool madeChanges;
            int temp;
            int count = fio.count();
            do
            {
                madeChanges = false;
                count--;
                for (int i = 0; i < count; i++)
                {
                    if (fio.get(i).CompareTo(fio.get(i + 1)) > 0)
                    {
                        temp = fio.get(i + 1);
                        fio.set(i + 1, fio.get(i));
                        fio.set(i, temp);
                        //fio.swap(i, i + 1);
                        madeChanges = true;
                    }
                }
            } while (madeChanges);
        }

        public void bubbleSortList(FioInt fio)
        {
            bool madeChanges;
            int temp;
            int count = fio.count();
            do
            {
                madeChanges = false;
                count--;
                for (int i = 0; i < count; i++)
                {
                    if (fio.goGet(i).CompareTo(fio.goGet(i + 1)) > 0)
                    {
                        temp = fio.goGet(i + 1);
                        fio.goSet(i + 1, fio.goGet(i));
                        fio.goSet(i, temp);
                        //fio.swap(i, i + 1);
                        madeChanges = true;
                    }
                }
            } while (madeChanges);
        }
    }
}
