﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmai
{
    class FioDouble : Fio
    {
        public FioDouble(String inputFile)
            : base(inputFile)
        {
        }

        public FioDouble(String inputFile, String outputFile)
            : base(inputFile, outputFile)
        {
        }

        public FioDouble(String inputFile, FileStream outputFile, StreamWriter outputStream)
            : base(inputFile, outputFile, outputStream)
        {
        }

        // Main work with array functions
        //###################################################################################################
        public double get(int position)
        {
            try
            {
                this.binaryReader.BaseStream.Seek(position * sizeof(double), System.IO.SeekOrigin.Begin);
                //Counting iterations
                if (this.countIterations) this.iterations += 1;
                return this.binaryReader.ReadDouble();
            }
            catch
            {
                return -1;
            }
        }

        public void set(int position, double number)
        {
            this.binaryWriter.Seek(position * sizeof(double), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void swap(int positionA, int positionB)
        {
            //Read elements
            this.binaryReader.BaseStream.Seek(positionA * sizeof(double), System.IO.SeekOrigin.Begin);
            double a = this.binaryReader.ReadDouble();
            this.binaryReader.BaseStream.Seek(positionB * sizeof(double), System.IO.SeekOrigin.Begin);
            double b = this.binaryReader.ReadDouble();
            //Write swaped elements
            this.binaryWriter.Seek(positionA * sizeof(double), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(b);
            this.binaryWriter.Seek(positionB * sizeof(double), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(a);
            //Counting iterations
            if (this.countIterations) this.iterations += 8;
        }

        public void remove(int position)
        {
            this.binaryWriter.Seek(position * sizeof(double), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0.0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        // Dynamic use
        public void declare(int size)
        {
            this.binaryWriter.Seek(size * sizeof(double), System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goFirst()
        {
            this.curentAddress = 0; // Put cursor to first position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goNext()
        {
            this.curentAddress += sizeof(double); // Put cursor to next position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goPrev()
        {
            this.curentAddress -= sizeof(double); // Put cursor to previous position
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
        }

        public void goPosition(int position)
        {
            if (this.optimized)
            {
                int currentPosition = this.curentAddress / sizeof(double);
                if (currentPosition < position)
                    for (int i = currentPosition; i != position; i++)
                        goNext();
                else if (currentPosition > position)
                    for (int i = currentPosition; i != position; i--)
                        goPrev();
            }
            else
            {
                goFirst();
                for (int i = 0; i < position; i++)
                    goNext();
            }
        }

        public double goGet()
        {
            try
            {
                this.binaryReader.BaseStream.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
                //Counting iterations
                if (this.countIterations) this.iterations += 2;
                return this.binaryReader.ReadDouble();
            }
            catch
            {
                return -1;
            }
        }

        public double goGet(int position)
        {
            goPosition(position);
            return goGet();
        }

        public void goRemove()
        {
            this.binaryWriter.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(0.0);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goRemove(int position)
        {
            goPosition(position);
            goRemove();
        }

        public void goSet(double number)
        {
            this.binaryWriter.Seek(this.curentAddress, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
        }

        public void goSet(int position, double number)
        {
            goPosition(position);
            goSet(number);
        }

        public void goPush(double number)
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            this.binaryWriter.Seek(length, System.IO.SeekOrigin.Begin);
            this.binaryWriter.Write(number);
            //Counting iterations
            if (this.countIterations) this.iterations += 3;
        }

        public bool goIsNextLast()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            int nextPosition = this.curentAddress + sizeof(double);
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return (length < nextPosition);
        }

        public bool goIsInPosition(int position)
        {
            int currentPosition = this.curentAddress / sizeof(double);
            //Counting iterations
            if (this.countIterations) this.iterations += 1;
            return (currentPosition == position);
        }

        // Misc functions
        public int countNonZeros()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            double element;

            int cursor = 0;
            int count = 0;
            while (length != 0)
            {
                this.binaryReader.BaseStream.Seek(cursor * sizeof(double), System.IO.SeekOrigin.Begin);
                element = this.binaryReader.ReadDouble();
                if (element != 0) { count++; }
                length -= sizeof(double);
                cursor++;
            }
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return count;
        }

        public int count()
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            //Counting iterations
            if (this.countIterations) this.iterations += 2;
            return length / sizeof(double);
        }

        public FioDouble join(FioDouble fioDouble)
        {
            for (fioDouble.goFirst(); !fioDouble.goIsNextLast(); fioDouble.goNext())
            {
                goPush(fioDouble.goGet());
            }
            return this;
        }

        public FioDouble rewrite(FioDouble fioDouble)
        {
            cleanWorkFile();
            for (fioDouble.goFirst(); !fioDouble.goIsNextLast(); fioDouble.goNext())
            {
                goPush(fioDouble.goGet());
            }
            return this;
        }

        // Generators
        public void generateArray(double min, double max, int count = 100, int round = 2)
        {
            cleanWorkFile();
            Random random = new Random();
            for (int i = 0; i < count; i++)
            {
                set(i, Math.Round(random.NextDouble() * (max - min) + min, round));

            }
        }

        // User data
        //###################################################################################################
        public void readUserFile(String file, char delimiter = '\n')
        {
            cleanWorkFile();
            using (TextReader reader = File.OpenText(file))
            {
                string text;
                string[] bits;
                while ((text = reader.ReadLine()) != null)
                {
                    bits = text.Split(delimiter);
                    foreach (string bit in bits)
                    {
                        goPush(double.Parse(bit));
                    }
                }
            }
        }

        public void readUserData()
        {
            cleanWorkFile();
            Console.WriteLine("Please enter count of numbers!");
            Console.Write("Count: ");
            int count = int.Parse(Console.ReadLine());

            for (int i = 0; i < count; i++)
            {
                Console.Write((i + 1) + "]");
                goPush(double.Parse(Console.ReadLine()));
            }
        }

        // Results
        //###################################################################################################
        public void print(bool withIndex = false)
        {
            int length = (int)this.binaryReader.BaseStream.Length;
            double element;

            int cursor = 0;

            smartWrite(this.tbp);
            while (length != 0)
            {
                this.binaryReader.BaseStream.Seek(cursor * sizeof(double), System.IO.SeekOrigin.Begin);
                element = this.binaryReader.ReadDouble();

                smartWrite(cursor.ToString() + ". ", withIndex && (((this.nozero) && (element != 0)) || (!this.nozero)));
                smartWrite(element.ToString() + "\r\n", (((this.nozero) && (element != 0)) || (!this.nozero)));

                length -= sizeof(double);
                cursor++;
            }
            smartWrite(this.tap);
        }

        public void printInRange(int begin, int end, bool withIndex = false)
        {
            smartWrite(this.tbp);
            for (int i = begin; i <= end; i++)
            {
                smartWrite(i.ToString() + ". ", withIndex && (((this.nozero) && (get(i) != 0)) || (!this.nozero)));
                smartWrite(get(i).ToString() + "\r\n", (((this.nozero) && (get(i) != 0)) || (!this.nozero)));
            }
            smartWrite(this.tap);
        }

    }
}
